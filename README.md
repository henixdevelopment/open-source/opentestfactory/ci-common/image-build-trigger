# OTF - Image Build trigger

This repository contains pipelines to trigger build image from other component's pipeline.
Thos common pipeline have to be used in addition to java-ci or python-ci according the language of the component.

## Trigger the build of otf allinone image

The common pipeline file to trigger the buiild of otf allinon image is :

* `allinone-build-trigger.yaml`

### Prerequisite

[TO complete]

* Variables needed
  * ENV_VERSION_KEY
  * COMPONENT_VERSION ( retrieved automatically through the hidden job `.retrieve-component-version`, see below)
  * OTF_IMAGE_PROJECT_ID ( defined in the pipeline )
  * OTF_IMAGE_REF_BRANCH ( defined in the pipeline )

* Hidden job `.retrieve-component-version` in the base pipeline for the language.


### Usage

To trigger the build of otf allinone image, one should add in it's component pipeline the lines below :

* For a project whose pipeline is base on java-ci

``` yaml

include:
  - project: 'mig-gitlab/ci-common/java-ci'
    ref: main
    file: 'gitlab-ci-java.yml'

# Lines to add >>>

  - project: 'mig-gitlab/ci-common/image-build-trigger'
    ref: main
    file: 'allinone-build-trigger.yaml'


variables:
  # Variable for allinone-build-trigger. The ENV_VERSION_KEY used here should match the env_version_key used for this component in the bom  otf-imaged.json (otf/imahes/otf-all-in-one-images)
  ENV_VERSION_KEY: "<COMPONENT_NAME_VERSION>"1

# <<< lines to add

```

* For a project whose pipeline is based on python-ci

``` yaml
include:
  - project: 'mig-gitlab/ci-common/java-ci'
    ref: main
    file: 'gitlab-ci-java.yml'

# Lines to add >>>

  - project: 'mig-gitlab/ci-common/image-build-trigger'
    ref: main
    file: 'allinone-build-trigger.yaml'


variables:
  # Variable for allinone-build-trigger. The ENV_VERSION_KEY used here should match the env_version_key used for this component in the bom  otf-imaged.json (otf/imahes/otf-all-in-one-images)
  ENV_VERSION_KEY: "<COMPONENT_NAME_VERSION>"

# <<< lines to add

```